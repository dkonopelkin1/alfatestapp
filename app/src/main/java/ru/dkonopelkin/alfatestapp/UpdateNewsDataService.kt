package ru.dkonopelkin.alfatestapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider
import ru.dkonopelkin.alfatestapp.ui.activity.NewsListActivity
import java.util.*

class UpdateNewsDataService : Service() {

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private val updateInterval: Long = 1000 * 60 * 10
    val repository = RepositoryProvider.sNewsRepository
    val intent = Intent(NewsListActivity.BROADCAST_ACTION_UPDATE)

    override fun onCreate() {
        super.onCreate()
        timer = Timer()
    }

    private fun startSchedule() {
        if (timerTask != null) {
            timerTask!!.cancel()
        }
        timerTask = object : TimerTask() {
            override fun run() {
                repository.getNews()
                        .subscribe({ data ->
                            if (!data.equals(repository.getCachedNews())) {
                                repository.cacheNews(data)
                                intent.putParcelableArrayListExtra(NewsListActivity.INTENT_NEWSLIST_DATA, data?.channel?.items)
                                sendBroadcast(intent)
                            }
                        }, {})
            }
        }
        timer!!.schedule(timerTask, updateInterval, updateInterval)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startSchedule()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder? {
        return Binder()
    }
}
