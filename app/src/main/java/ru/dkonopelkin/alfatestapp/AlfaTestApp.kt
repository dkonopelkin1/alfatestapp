package ru.dkonopelkin.alfatestapp

import android.app.Application
import com.orhanobut.hawk.Hawk
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider

class AlfaTestApp : Application() {
    override fun onCreate() {
        super.onCreate()
        RepositoryProvider.init()
        Hawk.init(this)
                .build()

    }
}