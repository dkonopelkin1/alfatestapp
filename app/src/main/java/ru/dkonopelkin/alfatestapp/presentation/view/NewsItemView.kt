package ru.dkonopelkin.alfatestapp.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface NewsItemView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onFavoritesStateChanged(isSaved: Boolean)
}
