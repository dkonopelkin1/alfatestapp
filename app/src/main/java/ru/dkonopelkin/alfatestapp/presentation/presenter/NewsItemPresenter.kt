package ru.dkonopelkin.alfatestapp.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider
import ru.dkonopelkin.alfatestapp.presentation.view.NewsItemView

@InjectViewState
class NewsItemPresenter : MvpPresenter<NewsItemView>() {

    fun notifyNewsState(item: Item) {
        if (RepositoryProvider.sNewsRepository.isNewsHasInFavorites(item)) {
            viewState.onFavoritesStateChanged(true)
        } else {
            viewState.onFavoritesStateChanged(false)
        }
    }

    fun changeNewsState(item: Item) {
        if (RepositoryProvider.sNewsRepository.isNewsHasInFavorites(item)) {
            RepositoryProvider.sNewsRepository.deleteNewsFromFavorites(item)
        } else {
            RepositoryProvider.sNewsRepository.saveNewsToFavorites(item)
        }
        notifyNewsState(item)
    }
}
