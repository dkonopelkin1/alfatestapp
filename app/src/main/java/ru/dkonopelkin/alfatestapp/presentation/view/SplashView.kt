package ru.dkonopelkin.alfatestapp.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.dkonopelkin.alfatestapp.data.models.Item

@StateStrategyType(SkipStrategy::class)
interface SplashView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onDataReceived(dataList: ArrayList<Item>?)

    @StateStrategyType(SkipStrategy::class)
    fun onError(data: String)

    fun showProgress()
    fun hideProgress()
}