package ru.dkonopelkin.alfatestapp.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider
import ru.dkonopelkin.alfatestapp.presentation.view.NewsListView

@InjectViewState
class NewsListPresenter : MvpPresenter<NewsListView>() {

    fun setData(dataList: ArrayList<Item>?) {
        viewState.onDataReceived(dataList)
    }

    fun setOfflineData() {
        viewState.hideProgress()
        viewState.onDataReceived(RepositoryProvider.sNewsRepository.getNewsListFromFavorites())
    }

    fun getNews() {
        viewState.showProgress()
        RepositoryProvider.sNewsRepository.getNews()
                .subscribe({ data ->
                    viewState.hideProgress()
                    RepositoryProvider.sNewsRepository.cacheNews(data)
                    viewState.onDataReceived(data.channel?.items)
                }
                        , { error: Throwable ->
                    viewState.onError(error.toString())
                    viewState.hideProgress()
                })
    }


}
