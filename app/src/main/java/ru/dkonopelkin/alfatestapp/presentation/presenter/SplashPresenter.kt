package ru.dkonopelkin.alfatestapp.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.dkonopelkin.alfatestapp.data.repositories.NewsRepository.DownloadState
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider
import ru.dkonopelkin.alfatestapp.presentation.view.SplashView

@InjectViewState
class SplashPresenter : MvpPresenter<SplashView>() {

    override fun attachView(view: SplashView?) {
        super.attachView(view)
        getData()
    }

    fun getData() {
        if (RepositoryProvider.sNewsRepository.getNewsDownloadState() == DownloadState.EMPTY) {
            viewState.showProgress()
            RepositoryProvider.sNewsRepository.getNews()
                    .subscribe({ data ->
                        viewState.hideProgress()
                        RepositoryProvider.sNewsRepository.cacheNews(data)
                        RepositoryProvider.sNewsRepository.setNewsDownloadState(DownloadState.DOWNLOADED)
                        viewState.onDataReceived(data?.channel?.items)
                    }
                            , { error: Throwable ->
                        viewState.onError(error.toString())
                        viewState.hideProgress()
                    })
        } else {
            viewState.onDataReceived(RepositoryProvider.sNewsRepository.getCachedNews()?.channel?.items)
        }
    }
}
