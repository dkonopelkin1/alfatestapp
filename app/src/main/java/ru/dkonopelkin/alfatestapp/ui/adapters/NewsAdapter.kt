package ru.dkonopelkin.alfatestapp.ui.adapters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.dkonopelkin.alfatestapp.R
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.ui.activity.NewsPagerActivity

class NewsAdapter(private val items: List<Item>,
                  private val context: Context,
                  val activity: Activity) : RecyclerView.Adapter<NewsAdapter.NewsItemViewHolder>() {

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        val newsItem = items[position]
        if (newsItem.title != null) {
            holder.txtVwNewsTitle.text = newsItem.title
        }
        holder.crdVwNewsItem.setOnClickListener({
            val arrayList: ArrayList<Item> = ArrayList(items.size)
            arrayList.addAll(items)
            val intent = NewsPagerActivity.getIntent(activity, arrayList, position)
            activity.startActivity(intent)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder {
        return NewsItemViewHolder(LayoutInflater.from(context).inflate(R.layout.news_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class NewsItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtVwNewsTitle = view.findViewById<TextView>(R.id.txtVwNewsTitle)!!
        val crdVwNewsItem = view.findViewById<CardView>(R.id.crdVwNewsItem)!!
    }
}