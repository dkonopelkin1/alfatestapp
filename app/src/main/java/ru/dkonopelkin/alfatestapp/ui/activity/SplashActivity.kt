package ru.dkonopelkin.alfatestapp.ui.activity

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.dkonopelkin.alfatestapp.R
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.presentation.presenter.SplashPresenter
import ru.dkonopelkin.alfatestapp.presentation.view.SplashView


class SplashActivity : MvpAppCompatActivity(), SplashView {
    @InjectPresenter
    lateinit var mSplashPresenter: SplashPresenter

    private var llLoading: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        llLoading = findViewById(R.id.llLoading)
    }


    override fun onDataReceived(dataList: ArrayList<Item>?) {
        val intent = NewsListActivity.getIntent(this, dataList)
        startActivity(intent)
    }


    override fun onError(data: String) {
        val onClickPositive: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, which ->
            dialog?.dismiss()
            onDataReceived(null)
        }
        val builder = AlertDialog.Builder(this)
                .setMessage(data)
                .setTitle(getString(R.string.string_error))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.string_ok), onClickPositive)
        val alert = builder.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    override fun showProgress() {
        llLoading?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        llLoading?.visibility = View.GONE
    }
}
