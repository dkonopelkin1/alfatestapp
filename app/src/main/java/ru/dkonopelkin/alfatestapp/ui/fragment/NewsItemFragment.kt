package ru.dkonopelkin.alfatestapp.ui.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.arellomobile.mvp.MvpAppCompatFragment
import ru.dkonopelkin.alfatestapp.R
import ru.dkonopelkin.alfatestapp.presentation.view.NewsItemView
import ru.dkonopelkin.alfatestapp.presentation.presenter.NewsItemPresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import ru.dkonopelkin.alfatestapp.data.models.Item

class NewsItemFragment : MvpAppCompatFragment(), NewsItemView {
    override fun onFavoritesStateChanged(isSaved: Boolean) {
        if (isSaved) {
            flBtnFavorites?.setImageResource(R.drawable.ic_star)
        } else {
            flBtnFavorites?.setImageResource(R.drawable.ic_star_border)
        }
    }

    companion object {
        const val ITEM_KEY = "ITEM_KEY"

        fun newInstance(): NewsItemFragment {
            return NewsItemFragment()
        }

        fun newInstance(items: Item): NewsItemFragment {
            val fragment = NewsItemFragment()
            val args = Bundle()
            args.putParcelable(ITEM_KEY, items)
            fragment.arguments = args
            return fragment
        }
    }

    var wbVwPage: WebView? = null
    var llLoading: View? = null
    var flBtnFavorites: FloatingActionButton? = null

    @InjectPresenter
    lateinit var mNewsItemPresenter: NewsItemPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wbVwPage = view.findViewById(R.id.wbVwPage)
        llLoading = view.findViewById(R.id.llLoading)
        flBtnFavorites = view.findViewById(R.id.flBtnFavorites)
        wbVwPage?.settings?.builtInZoomControls = true
        wbVwPage?.settings?.displayZoomControls = false
        wbVwPage?.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                llLoading?.visibility = View.GONE
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                llLoading?.visibility = View.VISIBLE
            }
        }
        if (arguments != null) {
            val data: Item? = arguments?.getParcelable(ITEM_KEY)
            if (data != null && data.description != null && wbVwPage != null) {
                mNewsItemPresenter.notifyNewsState(data)
                flBtnFavorites?.setOnClickListener({
                    mNewsItemPresenter.changeNewsState(data)
                })
                wbVwPage?.loadData(data.description, "text/html", null)
            }
        }
    }
}
