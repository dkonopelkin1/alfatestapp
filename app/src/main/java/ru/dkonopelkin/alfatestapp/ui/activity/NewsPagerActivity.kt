package ru.dkonopelkin.alfatestapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import com.arellomobile.mvp.MvpAppCompatActivity
import ru.dkonopelkin.alfatestapp.R
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.ui.fragment.NewsItemFragment

class NewsPagerActivity : MvpAppCompatActivity() {
    companion object {
        private val INTENT_ITEMS_DATA = "INTENT_ITEMS_DATA"
        private val INTENT_POSITION_KEY = "INTENT_POSITION_KEY"

        fun getIntent(context: Context, dataList: ArrayList<Item>, position: Int): Intent {
            val intent = Intent(context, NewsPagerActivity::class.java)
            intent.putParcelableArrayListExtra(INTENT_ITEMS_DATA, dataList)
            intent.putExtra(INTENT_POSITION_KEY, position)
            return intent
        }
    }

    private var vwPgrNews: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_pager)
        title = getString(R.string.title_news)
        vwPgrNews = findViewById(R.id.vwPgrNews)

        val items = intent.getParcelableArrayListExtra<Item>(INTENT_ITEMS_DATA)
        if (items != null) {
            val position = intent.getIntExtra(INTENT_POSITION_KEY, 0)
            setAdapter(supportFragmentManager, items, position)
        }
    }

    private fun setAdapter(fm: FragmentManager, list: List<Item>, position: Int) {
        val pagerAdapter = NewsPagerAdapter(fm, list)
        vwPgrNews?.adapter = pagerAdapter
        vwPgrNews?.currentItem = position
    }

    private inner class NewsPagerAdapter(fm: FragmentManager, val list: List<Item>) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            if (position < list.size) {
                val item: Item = list[position]
                return NewsItemFragment.newInstance(item)
            } else {
                return NewsItemFragment.newInstance()
            }
        }

        override fun getCount(): Int {
            return list.size
        }
    }
}
