package ru.dkonopelkin.alfatestapp.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.dkonopelkin.alfatestapp.R
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.presentation.presenter.NewsListPresenter
import ru.dkonopelkin.alfatestapp.presentation.view.NewsListView
import ru.dkonopelkin.alfatestapp.ui.adapters.NewsAdapter
import android.content.IntentFilter
import android.widget.Toast
import ru.dkonopelkin.alfatestapp.UpdateNewsDataService


class NewsListActivity : MvpAppCompatActivity(), NewsListView {

    companion object {

        private val INTENT_OFFLINE_ONLY = "INTENT_OFFLINE_ONLY"
        val INTENT_NEWSLIST_DATA = "INTENT_NEWSLIST_DATA"
        val BROADCAST_ACTION_UPDATE = "BROADCAST_ACTION_UPDATE"

        fun getIntent(context: Context, offlineOnly: Boolean): Intent {
            val intent = Intent(context, NewsListActivity::class.java)
            intent.putExtra(INTENT_OFFLINE_ONLY, offlineOnly)
            return intent
        }

        fun getIntent(context: Context, dataList: ArrayList<Item>?): Intent {
            val intent = Intent(context, NewsListActivity::class.java)
            intent.putParcelableArrayListExtra(INTENT_NEWSLIST_DATA, dataList)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    private var srlNews: SwipeRefreshLayout? = null
    private var rlVwNews: RecyclerView? = null
    private var llLoading: View? = null
    private var llEmpty: View? = null
    private var flBtnFavorites: FloatingActionButton? = null
    private var offlineOnly: Boolean = false
    private var brdCstReceiver: BroadcastReceiver? = null

    @InjectPresenter
    lateinit var mNewsListPresenter: NewsListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)

        llLoading = findViewById(R.id.llLoading)
        srlNews = findViewById(R.id.srlNews)
        rlVwNews = findViewById(R.id.rlVwNews)
        llEmpty = findViewById(R.id.llEmpty)
        flBtnFavorites = findViewById(R.id.flBtnFavorites)

        srlNews?.setOnRefreshListener {
            if (!offlineOnly) {
                mNewsListPresenter.getNews()
            } else {
                mNewsListPresenter.setOfflineData()
            }
        }
        srlNews?.setColorSchemeResources(R.color.colorPrimary)

        offlineOnly = intent.getBooleanExtra(INTENT_OFFLINE_ONLY, false)
        if (!offlineOnly) {
            title = getString(R.string.title_news_list)
            val data = intent.getParcelableArrayListExtra<Item>(INTENT_NEWSLIST_DATA)
            mNewsListPresenter.setData(data)

            flBtnFavorites?.setOnClickListener({
                val intent = NewsListActivity.getIntent(this, true)
                startActivity(intent)
            })
            brdCstReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    if (intent != null) {
                        Toast.makeText(this@NewsListActivity, getString(R.string.data_is_updated), Toast.LENGTH_LONG).show()
                        val dataList = intent.getParcelableArrayListExtra<Item>(INTENT_NEWSLIST_DATA)
                        if (dataList != null) {
                            mNewsListPresenter.setData(dataList)
                        }
                    }
                }
            }
            val intentFilter = IntentFilter(BROADCAST_ACTION_UPDATE)
            registerReceiver(brdCstReceiver, intentFilter)
            startUpdateService()
        } else {
            title = getString(R.string.title_saved_news)
            mNewsListPresenter.setOfflineData()
            flBtnFavorites?.visibility = View.GONE
        }
    }

    override fun onDataReceived(dataList: ArrayList<Item>?) {
        if (rlVwNews != null && dataList != null && dataList.size > 0) {
            val adapter = NewsAdapter(dataList, applicationContext, this)
            val layoutManager = LinearLayoutManager(applicationContext)
            rlVwNews?.layoutManager = layoutManager
            rlVwNews?.adapter = adapter
            hideEmptyList()
        } else {
            showEmptyList()
        }
    }

    override fun onError(data: String) {
        val onClickPositive: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, which ->
            dialog?.dismiss()
        }
        val builder = AlertDialog.Builder(this)
                .setMessage(data)
                .setTitle(getString(R.string.string_error))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.string_ok), onClickPositive)
        val alert = builder.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    override fun showProgress() {
        llLoading?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        llLoading?.visibility = View.GONE
        srlNews?.isRefreshing = false
    }

    private fun showEmptyList() {
        llEmpty?.visibility = View.VISIBLE
        rlVwNews?.visibility = View.GONE
    }

    private fun hideEmptyList() {
        llEmpty?.visibility = View.GONE
        rlVwNews?.visibility = View.VISIBLE
    }

    private fun startUpdateService() {
        val intent = Intent(this, UpdateNewsDataService::class.java)
        startService(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (brdCstReceiver != null) {
            try {
                unregisterReceiver(brdCstReceiver)
            } catch (exception: IllegalArgumentException) {
                Log.d("UpdateBroadcastReceiver", "receiver is unregistred")
            }
        }
    }
}
