package ru.dkonopelkin.alfatestapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss")
public class Rss implements Parcelable {

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Element(name = "channel", required = false)
    private Channel channel;

    @Attribute(name = "version", required = false)
    private String version;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.channel, flags);
        dest.writeString(this.version);
    }

    public Rss() {
    }

    protected Rss(Parcel in) {
        this.channel = in.readParcelable(Channel.class.getClassLoader());
        this.version = in.readString();
    }

    public static final Parcelable.Creator<Rss> CREATOR = new Parcelable.Creator<Rss>() {
        @Override
        public Rss createFromParcel(Parcel source) {
            return new Rss(source);
        }

        @Override
        public Rss[] newArray(int size) {
            return new Rss[size];
        }
    };

    public Channel getChannel() {
        return channel;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rss)) return false;

        Rss rss = (Rss) o;

        if (channel != null ? !channel.equals(rss.channel) : rss.channel != null) return false;
        return version != null ? version.equals(rss.version) : rss.version == null;
    }

    @Override
    public int hashCode() {
        int result = channel != null ? channel.hashCode() : 0;
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
