package ru.dkonopelkin.alfatestapp.data.repositories

import io.reactivex.Observable
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.data.models.Rss

interface INewsRepository {
    fun getNews(): Observable<Rss>
    fun cacheNews(data: Rss?)
    fun getCachedNews(): Rss?
    fun setNewsDownloadState(state: NewsRepository.DownloadState)
    fun getNewsDownloadState(): NewsRepository.DownloadState
    fun saveNewsToFavorites(item: Item)
    fun getNewsListFromFavorites() : ArrayList<Item>
    fun deleteNewsFromFavorites(item: Item)
    fun isNewsHasInFavorites(item : Item) :Boolean
}