package ru.dkonopelkin.alfatestapp.data.repositories

import ru.dkonopelkin.alfatestapp.data.apiServices.NewsApiService

class RepositoryProvider {
    companion object {
        lateinit var sNewsRepository: INewsRepository

        fun init() {
            sNewsRepository = NewsRepository(NewsApiService.create())
        }
    }
}