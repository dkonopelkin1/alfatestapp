package ru.dkonopelkin.alfatestapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "channel")
public class Channel implements Parcelable {

    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    @Element(name = "description")
    private String description;

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "language")
    private String language;

    @Element(name = "docs")
    private String docs;

    @Element(name = "managingEditor")
    private String managingEditor;

    @Element(name = "webMaster")
    private String webMaster;

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    @ElementList(inline = true, required = false)
    private ArrayList<Item> items;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.link);
        dest.writeString(this.description);
        dest.writeString(this.pubDate);
        dest.writeString(this.language);
        dest.writeString(this.docs);
        dest.writeString(this.managingEditor);
        dest.writeString(this.webMaster);
        dest.writeTypedList(this.items);
    }

    public Channel() {
    }

    protected Channel(Parcel in) {
        this.title = in.readString();
        this.link = in.readString();
        this.description = in.readString();
        this.pubDate = in.readString();
        this.language = in.readString();
        this.docs = in.readString();
        this.managingEditor = in.readString();
        this.webMaster = in.readString();
        this.items = in.createTypedArrayList(Item.CREATOR);
    }

    public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel source) {
            return new Channel(source);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getLanguage() {
        return language;
    }

    public String getDocs() {
        return docs;
    }

    public String getManagingEditor() {
        return managingEditor;
    }

    public String getWebMaster() {
        return webMaster;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Channel)) return false;

        Channel channel = (Channel) o;

        if (title != null ? !title.equals(channel.title) : channel.title != null) return false;
        if (link != null ? !link.equals(channel.link) : channel.link != null) return false;
        if (description != null ? !description.equals(channel.description) : channel.description != null)
            return false;
        if (pubDate != null ? !pubDate.equals(channel.pubDate) : channel.pubDate != null)
            return false;
        if (language != null ? !language.equals(channel.language) : channel.language != null)
            return false;
        if (docs != null ? !docs.equals(channel.docs) : channel.docs != null) return false;
        if (managingEditor != null ? !managingEditor.equals(channel.managingEditor) : channel.managingEditor != null)
            return false;
        if (webMaster != null ? !webMaster.equals(channel.webMaster) : channel.webMaster != null)
            return false;
        return items != null ? items.equals(channel.items) : channel.items == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (pubDate != null ? pubDate.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (docs != null ? docs.hashCode() : 0);
        result = 31 * result + (managingEditor != null ? managingEditor.hashCode() : 0);
        result = 31 * result + (webMaster != null ? webMaster.hashCode() : 0);
        result = 31 * result + (items != null ? items.hashCode() : 0);
        return result;
    }
}
