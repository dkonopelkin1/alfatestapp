package ru.dkonopelkin.alfatestapp.data.apiServices

import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.dkonopelkin.alfatestapp.data.models.Rss

interface NewsApiService {

    @GET("_rss.html")
    fun getNewsRss(@Query("subtype") subtype: String,
                   @Query("category") category: String,
                   @Query("city") city: String): Observable<Rss>

    companion object Factory {
        fun create(url: String = "https://alfabank.ru/_/rss/"): NewsApiService {
            val retrofit = Retrofit.Builder()
                    .baseUrl(url)
                    .client(OkHttpClient())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofit.create(NewsApiService::class.java)
        }
    }

}