package ru.dkonopelkin.alfatestapp.data.repositories

import com.orhanobut.hawk.Hawk
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.dkonopelkin.alfatestapp.data.apiServices.NewsApiService
import ru.dkonopelkin.alfatestapp.data.models.Item
import ru.dkonopelkin.alfatestapp.data.models.Rss

class NewsRepository(private val newsApiService: NewsApiService) : INewsRepository {

    enum class DownloadState {
        EMPTY, DOWNLOADED
    }

    private val IS_NEWS_DOWNLOADED_KEY = "IS_NEWS_DOWNLOADED_KEY"
    private val RSS_DATA_KEY = "RSS_DATA_KEY"
    private val FAVORITES_LIST_KEY = "FAVORITES_LIST_KEY"

    override fun getNews(): Observable<Rss> {
        return newsApiService.getNewsRss("1", "2", "21")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun cacheNews(data: Rss?) {
        Hawk.put(RSS_DATA_KEY, data)
    }

    override fun getCachedNews(): Rss? {
        return Hawk.get(RSS_DATA_KEY, null)
    }

    override fun setNewsDownloadState(state: DownloadState) {
        Hawk.put(IS_NEWS_DOWNLOADED_KEY, state)
    }

    override fun getNewsDownloadState(): DownloadState {
        return Hawk.get(IS_NEWS_DOWNLOADED_KEY, DownloadState.EMPTY)
    }

    override fun saveNewsToFavorites(item: Item) {
        val itemsSet = Hawk.get(FAVORITES_LIST_KEY, HashSet<Item>())
        itemsSet.add(item)
        Hawk.put(FAVORITES_LIST_KEY, itemsSet)
    }

    override fun getNewsListFromFavorites(): ArrayList<Item> {
        val itemsSet = Hawk.get(FAVORITES_LIST_KEY, HashSet<Item>())
        val arrayList = ArrayList<Item>()
        arrayList.addAll(itemsSet.toList())
        return arrayList
    }

    override fun deleteNewsFromFavorites(item: Item) {
        val itemsSet = Hawk.get(FAVORITES_LIST_KEY, HashSet<Item>())
        itemsSet.remove(item)
        Hawk.put(FAVORITES_LIST_KEY, itemsSet)
    }

    override fun isNewsHasInFavorites(item: Item): Boolean {
        val itemsSet = Hawk.get(FAVORITES_LIST_KEY, HashSet<Item>())
        return itemsSet.contains(item)
    }
}