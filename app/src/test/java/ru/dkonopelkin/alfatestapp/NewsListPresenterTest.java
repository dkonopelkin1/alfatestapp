package ru.dkonopelkin.alfatestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import ru.dkonopelkin.alfatestapp.data.models.Channel;
import ru.dkonopelkin.alfatestapp.data.models.Item;
import ru.dkonopelkin.alfatestapp.data.models.Rss;
import ru.dkonopelkin.alfatestapp.data.repositories.NewsRepository;
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider;
import ru.dkonopelkin.alfatestapp.presentation.presenter.NewsListPresenter;
import ru.dkonopelkin.alfatestapp.presentation.view.NewsListView;
import ru.dkonopelkin.alfatestapp.presentation.view.NewsListView$$State;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@RunWith(JUnit4.class)
public class NewsListPresenterTest {

    private NewsListPresenter mPresenter;

    @Mock
    private
    NewsListView mView;

    @Mock
    private
    NewsListView$$State mViewState;

    private Rss testRss = new Rss();
    private Channel testChannel = new Channel();
    private Item item1 = new Item();
    private Item item2 = new Item();
    private ArrayList<Item> testList = new ArrayList<>();
    private ArrayList<Item> offlineList = new ArrayList<>();

    @Before
    public void setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());

        RepositoryProvider.Companion.init();

        MockitoAnnotations.initMocks(this);

        item1.setGuid("1");
        item2.setGuid("2");
        testList.add(item1);
        testList.add(item2);
        offlineList.add(item2);
        testChannel.setItems(testList);
        testRss.setChannel(testChannel);

        NewsRepository testRepository = mock(NewsRepository.class);
        when(testRepository.getNewsListFromFavorites()).thenReturn(offlineList);
        when(testRepository.getNews()).thenReturn(Observable.just(testRss));
        RepositoryProvider.Companion.setSNewsRepository(testRepository);

        mPresenter = new NewsListPresenter();
        mPresenter.setViewState(mViewState);
        mPresenter.attachView(mView);
    }

    @Test
    public void testCreated() {
        assertNotNull(mPresenter);
    }

    @Test
    public void testNoActionsWithView() {
        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testSetData() {
        mPresenter.setData(testList);
        verify(mViewState).onDataReceived(testList);
    }

    @Test
    public void testSetOfflineData() {
        mPresenter.setOfflineData();
        verify(mViewState).onDataReceived(offlineList);
    }
    @Test
    public void testNetworkDataReceived() {
        mPresenter.getNews();
        verify(mViewState).showProgress();
        verify(mViewState).hideProgress();
        verify(mViewState).onDataReceived(testList);
    }

    @Test
    public void testInvalidData() {
        NewsRepository testInvalidRepository = mock(NewsRepository.class);
        when(testInvalidRepository.getNews()).thenReturn(Observable.error(new Throwable()));
        RepositoryProvider.Companion.setSNewsRepository(testInvalidRepository);

        mPresenter.getNews();
        verify(mViewState).showProgress();
        verify(mViewState).hideProgress();
        verify(mViewState).onError(any());
    }

}