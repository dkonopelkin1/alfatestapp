package ru.dkonopelkin.alfatestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import ru.dkonopelkin.alfatestapp.data.models.Channel;
import ru.dkonopelkin.alfatestapp.data.models.Item;
import ru.dkonopelkin.alfatestapp.data.models.Rss;
import ru.dkonopelkin.alfatestapp.data.repositories.NewsRepository;
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider;
import ru.dkonopelkin.alfatestapp.presentation.presenter.SplashPresenter;
import ru.dkonopelkin.alfatestapp.presentation.view.SplashView;
import ru.dkonopelkin.alfatestapp.presentation.view.SplashView$$State;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@RunWith(JUnit4.class)
public class SplashPresenterTest {

    private SplashPresenter mPresenter;

    @Mock
    private
    SplashView mView;

    @Mock
    private
    SplashView$$State mViewState;

    private Rss testRss = new Rss();
    private Channel testChannel = new Channel();
    private Item item1 = new Item();
    private Item item2 = new Item();
    private ArrayList<Item> testList = new ArrayList<>();

    @Before
    public void setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());

        RepositoryProvider.Companion.init();

        MockitoAnnotations.initMocks(this);

        mPresenter = new SplashPresenter();

        item1.setGuid("1");
        item2.setGuid("2");
        testList.add(item1);
        testList.add(item2);
        testChannel.setItems(testList);
        testRss.setChannel(testChannel);

        NewsRepository testRepository = mock(NewsRepository.class);
        when(testRepository.getNewsDownloadState()).thenReturn(NewsRepository.DownloadState.EMPTY);
        RepositoryProvider.Companion.setSNewsRepository(testRepository);
        //first time, on view attach, return empty RSS
        when(testRepository.getNews()).thenReturn(Observable.just(new Rss()));

        mPresenter.setViewState(mViewState);
        mPresenter.attachView(mView);

        when(testRepository.getNews()).thenReturn(Observable.just(testRss));

    }

    @Test
    public void testCreated() {
        assertNotNull(mPresenter);
    }

    @Test
    public void testNoActionsWithView() {
        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testNetworkDataReceived() {
        mPresenter.getData();
        verify(mViewState, times(2)).showProgress(); //first invocation onAttachView
        verify(mViewState, times(2)).hideProgress();
        verify(mViewState).onDataReceived(testList);
    }

    @Test
    public void testCachedData() {
        NewsRepository testCacheRepository = mock(NewsRepository.class);
        when(testCacheRepository.getNewsDownloadState()).thenReturn(NewsRepository.DownloadState.DOWNLOADED);
        when(testCacheRepository.getCachedNews()).thenReturn(testRss);

        RepositoryProvider.Companion.setSNewsRepository(testCacheRepository);

        mPresenter.getData();
        verify(mViewState).onDataReceived(testList);
    }

    @Test
    public void testInvalidData() {
        NewsRepository testInvalidRepository = mock(NewsRepository.class);
        when(testInvalidRepository.getNewsDownloadState()).thenReturn(NewsRepository.DownloadState.EMPTY);
        when(testInvalidRepository.getNews()).thenReturn(Observable.error(new Throwable()));

        RepositoryProvider.Companion.setSNewsRepository(testInvalidRepository);

        mPresenter.getData();
        verify(mViewState, times(2)).showProgress(); //first invocation onAttachView
        verify(mViewState, times(2)).hideProgress();
        verify(mViewState).onError(any());
    }
}