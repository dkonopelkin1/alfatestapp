package ru.dkonopelkin.alfatestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import ru.dkonopelkin.alfatestapp.data.models.Item;
import ru.dkonopelkin.alfatestapp.data.repositories.NewsRepository;
import ru.dkonopelkin.alfatestapp.data.repositories.RepositoryProvider;
import ru.dkonopelkin.alfatestapp.presentation.presenter.NewsItemPresenter;
import ru.dkonopelkin.alfatestapp.presentation.view.NewsItemView;
import ru.dkonopelkin.alfatestapp.presentation.view.NewsItemView$$State;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@RunWith(JUnit4.class)
public class NewsItemPresenterTest {

    private NewsItemPresenter mPresenter;

    @Mock
    private
    NewsItemView mView;

    @Mock
    private
    NewsItemView$$State mViewState;


    private Item favoritesItem = new Item("favoritestitle",
            "favoriteslink",
            "favoritesdescription",
            "favoritespubDate",
            "favoritesguid");
    private Item onlineItem = new Item("title",
            "link",
            "description",
            "pubDate",
            "guid");

    @Before
    public void setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());

        RepositoryProvider.Companion.init();

        MockitoAnnotations.initMocks(this);

        NewsRepository testRepository = mock(NewsRepository.class);
        when(testRepository.isNewsHasInFavorites(favoritesItem)).thenReturn(true);
        when(testRepository.isNewsHasInFavorites(onlineItem)).thenReturn(false);
        doNothing().when(testRepository).deleteNewsFromFavorites(any());
        doNothing().when(testRepository).saveNewsToFavorites(any());
        RepositoryProvider.Companion.setSNewsRepository(testRepository);

        mPresenter = new NewsItemPresenter();
        mPresenter.setViewState(mViewState);
        mPresenter.attachView(mView);
    }

    @Test
    public void testCreated() {
        assertNotNull(mPresenter);
    }

    @Test
    public void testNoActionsWithView() {
        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testNotifyNewsState() {
        mPresenter.notifyNewsState(favoritesItem);
        verify(mViewState).onFavoritesStateChanged(true);
        mPresenter.notifyNewsState(onlineItem);
        verify(mViewState).onFavoritesStateChanged(false);
    }

    @Test
    public void testChangeNewsState() {
        mPresenter.changeNewsState(favoritesItem);
        verify(mViewState).onFavoritesStateChanged(true);
        mPresenter.changeNewsState(onlineItem);
        verify(mViewState).onFavoritesStateChanged(false);
    }
}